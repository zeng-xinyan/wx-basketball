package com.example.wx_basketball.controller;

import com.example.wx_basketball.entity.Admin;
import com.example.wx_basketball.service.AdminService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * Author  zxy
 * Date  2023/3/11 上午 9:08
 */
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Resource
    private AdminService adminService;

    @PostMapping("/login")
    public Map<Object, Object> loginIn(String loginName, String password) {
        Map<Object, Object> map = new HashMap<>();

        Admin admin = adminService.getAdminByLoginName(loginName);

        // 用户名不存在
        if (admin == null) {
            map.put("code", 500);
            map.put("result", "用户名错误");
            System.out.println("用户名错误！");
        }
        else {
            String pw = admin.getPassword();
            // 密码错误
            if (!pw.equals(password)) {
                map.put("code", 400);
                map.put("result", "密码错误");
                System.out.println("密码错误！");
            }
            // 登录成功
            else {
                map.put("code", 200);
                map.put("result", "登录成功");
                map.put("userId", -1);
                System.out.println("登录成功！");
            }
        }
        return map;
    }

}
