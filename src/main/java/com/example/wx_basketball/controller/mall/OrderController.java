package com.example.wx_basketball.controller.mall;

import com.example.wx_basketball.service.mall.OrderService;
import com.example.wx_basketball.service.mall.ShoppingCartService;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@RequestMapping("/order")
@RestController
public class OrderController {

    @Resource
    OrderService orderService;

    @Resource
    ShoppingCartService shoppingCartService;

    @PostMapping("/add")
    public void add(@RequestBody Map<String, ArrayList> orderItem) {
        // 将前端传递的数据 [{"id": xx, "count": xxx}] 存进数据库
        ArrayList items = orderItem.get("orderItem");
        ArrayList userId = orderItem.get("userId");

        System.out.println(items);
        System.out.println(userId);

        LinkedHashMap ii = (LinkedHashMap) userId.get(0);
        Integer integer = (Integer) ii.get("id");
        int id = integer.intValue();

        // 步骤一：对用户提交的订单进行保存
        // 数据库存储字段声明为 json，因此要用 new JSONArray 方法，将接收的数组转化成 json 类型。
        // 又因为 Java 对应 数据库 json 字段为 String 类型，于是要转化成 String 字段往 Service 传递
        orderService.add(String.valueOf(new JSONArray(items)), id);

        // 步骤二：删除用户购物车中已经下单的数据
        for (int i = 0; i < items.size(); i++) {
            Map map = (Map) items.get(i);
            int productId = (int) map.get("productId");
            shoppingCartService.deleteByProductId(productId, id);
        }
    }

    @GetMapping("/list")
    public List<List<Map<String, Object>>> list(int userId) {
        return orderService.list(userId);
    }

}