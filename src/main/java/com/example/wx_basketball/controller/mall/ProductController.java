package com.example.wx_basketball.controller.mall;

import com.example.wx_basketball.entity.mall.Product;
import com.example.wx_basketball.service.mall.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/9 上午 9:54
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    @Resource
    ProductService productService;

    @GetMapping("/list")
    public List<Product> getProducts() {
        return productService.getProducts();
    }

    @GetMapping("/{productId}")
    public Product getProductById(@PathVariable("productId") int id) {
        return productService.getProductById(id);
    }

}
