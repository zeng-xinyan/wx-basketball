package com.example.wx_basketball.controller;

import com.example.wx_basketball.entity.News;
import com.example.wx_basketball.service.NewsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.xml.crypto.Data;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/8 下午 1:28
 */
@RestController
public class NewsController {

    @Resource
    NewsService newsService;

    @GetMapping("/getNewsByNewsId")
    public News getNewsByNewsId (int newsId) {
        return newsService.getNewsByNewsId(newsId);
    }

    @GetMapping("/getAllNews")
    public List<News> getAllnews () {
        List<News> allNews = newsService.getAllNews();
        Collections.reverse(allNews);
        return allNews;
    }

    @PostMapping("/addNews")
    public void addNews(String title, String image, String detail) {
        String author = "admin";
        Date publishDate = new Date();
        newsService.insertNews(title, author, publishDate, image, detail);
    }

    @GetMapping("/deleteNewsByNewsId")
    public int deleteNewsByNewsId(int newsId) {
        return newsService.deleteNews(newsId);
    }

}
