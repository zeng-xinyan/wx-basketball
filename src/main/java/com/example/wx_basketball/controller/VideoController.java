package com.example.wx_basketball.controller;

import com.example.wx_basketball.entity.Video;
import com.example.wx_basketball.service.VideoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/8 下午 4:22
 */
@RestController
public class VideoController {

    @Resource
    private VideoService videoService;


    @GetMapping("/getAllVideos")
    List<Video> getAllVideos() {
        return videoService.getAllVideos();
    }

    @GetMapping("/getByVideoId")
    Video getByVideoId(int videoId) {
        return videoService.getByVideoId(videoId);
    }

}
