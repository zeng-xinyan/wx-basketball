package com.example.wx_basketball.controller;

import com.example.wx_basketball.entity.UserDynamic;
import com.example.wx_basketball.service.UserDynamicService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/7 下午 12:49
 */
@RestController
public class UserDynamicController {

    @Resource
    private UserDynamicService userDynamicService;

    @PostMapping("/addUserDynamic")
    public int addUserDynamic(int userId, String dynamic) {
        return userDynamicService.insertUserDynamic(userId, dynamic);
    }

    @GetMapping("/getUserDynamic")
    public List<UserDynamic> getUserDynamic(int userId) {
        return userDynamicService.getByUserId(userId);
    }

    @GetMapping("/deleteUserDynamicByUserDynamicId")
    public int deleteUserDynamicByUserDynamicId(Integer userDynamicId) {
        System.out.println(userDynamicId);
        return userDynamicService.deleteByUserDynamicId(userDynamicId);
    }

    @GetMapping("/updateUserDynamicByUserDynamicId")
    public int updateUserDynamicByUserDynamicId(int userDynamicId, String dynamic) {
        return userDynamicService.updateByUserDynamicId(userDynamicId, dynamic);
    }

}
