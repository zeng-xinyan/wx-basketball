package com.example.wx_basketball.controller;

import com.example.wx_basketball.entity.Live;
import com.example.wx_basketball.entity.Video;
import com.example.wx_basketball.service.LiveService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/8 下午 7:36
 */
@RestController
public class LiveController {

    @Resource
    private LiveService liveService;

    @GetMapping("/getAllLives")
    List<Live> getAllVideos() {
        return liveService.selectAllLives();
    }

    @GetMapping("/getByLiveId")
    Live getByVideoId(int videoId) {
        Live live = liveService.selectByLiveId(videoId);
        System.out.println(live.toString());
        return live;
    }

}
