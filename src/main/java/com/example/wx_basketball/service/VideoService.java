package com.example.wx_basketball.service;

import com.example.wx_basketball.entity.Video;
import com.example.wx_basketball.mapper.VideoMapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/8 下午 4:20
 */
@Service
public class VideoService {

    @Resource
    VideoMapper videoMapper;

    public List<Video> getAllVideos() {
        return videoMapper.selectAllVideos();
    }

    public Video getByVideoId(int videoId) {
        return videoMapper.selectByVideoId(videoId);
    }

}
