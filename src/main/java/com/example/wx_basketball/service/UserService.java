package com.example.wx_basketball.service;

import com.example.wx_basketball.entity.User;
import com.example.wx_basketball.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Author  zxy
 * Date  2023/3/6 下午 10:30
 */
@Service
public class UserService {

    @Resource
    private UserMapper userMapper;

    public User getUserByLoginName(String loginName) {
        User user = userMapper.selectByLoginName(loginName);
        // System.out.println(user.toString());
        return user;
    }

    public User getUserByUserId(int userId) {
        User user = userMapper.selectByUserId(userId);
        // System.out.println(user.toString());
        return user;
    }


    public int register(String loginName, String password) {
        int id = userMapper.insertUser(loginName, password);
        return id;
    }

    public int updateUserInfo(String loginName, String password, String introduce, int userId) {
        return userMapper.updateUserInfo(loginName, password, introduce, userId);
    }
}
