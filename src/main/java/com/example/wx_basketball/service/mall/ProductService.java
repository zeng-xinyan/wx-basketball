package com.example.wx_basketball.service.mall;

import com.example.wx_basketball.entity.mall.Product;
import com.example.wx_basketball.mapper.mall.ProductMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/9 上午 9:59
 */
@Service
public class ProductService {

    @Resource
    ProductMapper productMapper;

    public List<Product> getProducts() {
        return productMapper.getProducts();
    }

    public Product getProductById(int id) {
        return productMapper.getProductById(id);
    }

}
