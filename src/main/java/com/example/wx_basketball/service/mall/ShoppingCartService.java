package com.example.wx_basketball.service.mall;

import com.example.wx_basketball.entity.mall.ShoppingCart;
import com.example.wx_basketball.mapper.mall.ShoppingCartMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class ShoppingCartService {

    @Resource
    ShoppingCartMapper shoppingCartMapper;

    public ShoppingCart getByProductId(int productId, int userId) {
        return shoppingCartMapper.getByProductId(productId, userId);
    }

    public void add(int productId, int userId) {
        shoppingCartMapper.add(productId, userId);
    }

    public void updateCount(int productId, int userId) {
        shoppingCartMapper.updateCount(productId, userId);
    }

    public void cutCount(int productId, int userId) {
        shoppingCartMapper.cutCount(productId, userId);
    }

    public void deleteByProductId(int productId, int userId) {
        shoppingCartMapper.deleteByProductId(productId, userId);
    }

    public List getShoppingCartByUid(int userId) {
        List products =  shoppingCartMapper.getShoppingCartByUid(userId);
        return shoppingCartMapper.getShoppingCartByUid(userId);
    }
}
