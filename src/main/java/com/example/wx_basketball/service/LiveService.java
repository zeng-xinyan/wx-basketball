package com.example.wx_basketball.service;

import com.example.wx_basketball.entity.Live;
import com.example.wx_basketball.mapper.LiveMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/8 下午 7:34
 */
@Service
public class LiveService {

    @Resource
    private LiveMapper liveMapper;

    public Live selectByLiveId(int liveId) {
        return liveMapper.selectByLiveId(liveId);
    }

    public List<Live> selectAllLives() {
        return liveMapper.selectAllLives();
    }


}
