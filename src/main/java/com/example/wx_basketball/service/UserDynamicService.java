package com.example.wx_basketball.service;

import com.example.wx_basketball.entity.UserDynamic;
import com.example.wx_basketball.mapper.UserDynamicMapper;
import com.example.wx_basketball.mapper.UserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/7 下午 12:43
 */
@Service
public class UserDynamicService {

    @Resource
    UserDynamicMapper userDynamicMapper;

    public UserDynamic getByUserDynamicId(int userDynamicId) {
        return userDynamicMapper.getByUserDynamicId(userDynamicId);
    }

    public List<UserDynamic> getByUserId(int userId) {
        return userDynamicMapper.getByUserId(userId);
    }

    public int insertUserDynamic(int userId, String dynamic) {
        Date date = new Date();
        return userDynamicMapper.insert(userId, dynamic, date);
    }

    public int deleteByUserDynamicId(Integer userDynamicId) {
        return userDynamicMapper.deleteByUserDynamicId(userDynamicId);
    }

    public int updateByUserDynamicId(int userDynamicId, String dynamic) {
        return userDynamicMapper.updateByUserDynamicId(userDynamicId, dynamic);
    }

}
