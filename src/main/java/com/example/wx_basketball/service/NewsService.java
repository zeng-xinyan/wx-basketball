package com.example.wx_basketball.service;

import com.example.wx_basketball.entity.News;
import com.example.wx_basketball.mapper.NewsMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/8 下午 1:26
 */
@Service
public class NewsService {

    @Resource
    NewsMapper newsMapper;

    public News getNewsByNewsId(int newsId) {
        return newsMapper.selectById(newsId);
    }

    public List<News> getAllNews() {
        return newsMapper.selectAll();
    }

    public void insertNews(String title, String author, Date publishDate, String image, String detail) {
         newsMapper.insertNews(title, author, publishDate, image, detail);
    }

    public int deleteNews(int newsId) {
        return newsMapper.deleteByNewsId(newsId);
    }

}
