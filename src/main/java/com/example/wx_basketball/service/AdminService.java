package com.example.wx_basketball.service;

import com.example.wx_basketball.entity.Admin;
import com.example.wx_basketball.mapper.AdminMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Author  zxy
 * Date  2023/3/11 上午 9:11
 */
@Service
public class AdminService {

    @Resource
    private AdminMapper adminMapper;

    public Admin getAdminByLoginName(String loginName) {
        return adminMapper.selectByLoginName(loginName);
    }

}
