package com.example.wx_basketball.mapper;

import com.example.wx_basketball.entity.Live;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/8 下午 7:32
 */
@Mapper
public interface LiveMapper {

    @Select("SELECT * FROM lives")
    List<Live> selectAllLives ();

    @Select("SELECT * FROM lives WHERE live_id = #{liveId}")
    Live selectByLiveId(@Param("liveId") int liveId);

}
