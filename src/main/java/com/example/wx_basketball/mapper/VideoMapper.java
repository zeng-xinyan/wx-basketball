package com.example.wx_basketball.mapper;

import com.example.wx_basketball.entity.Video;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/8 下午 4:18
 */
@Mapper
public interface VideoMapper {

    @Select("SELECT * FROM videos")
    List<Video> selectAllVideos ();

    @Select("SELECT * FROM videos WHERE video_id = #{videoId}")
    Video selectByVideoId(@Param("videoId") int videoId);

}
