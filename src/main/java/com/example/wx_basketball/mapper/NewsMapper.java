package com.example.wx_basketball.mapper;

import com.example.wx_basketball.entity.News;
import org.apache.ibatis.annotations.*;

import java.util.Date;
import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/8 下午 1:24
 */
@Mapper
public interface NewsMapper {

    @Select("SELECT * FROM news WHERE news_id = #{newsId}")
    News selectById(@Param("newsId") int newsId);

    @Select("SELECT * FROM news")
    List<News> selectAll();

    @Insert("INSERT INTO wx_basketball.news(title, author, publish_date, image, detail) VALUES (#{title}, #{author}, #{publishDate}, #{image}, #{detail})")
    void insertNews(@Param("title") String title, @Param("author") String author, @Param("publishDate") Date publishDate, @Param("image") String image, @Param("detail") String detail);

    @Delete("DELETE FROM news WHERE news_id = #{newsId}")
    int deleteByNewsId(@Param("newsId") int newsId);

}
