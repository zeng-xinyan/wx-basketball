package com.example.wx_basketball.mapper;

import com.example.wx_basketball.entity.User;
import org.apache.ibatis.annotations.*;

/**
 * Author  zxy
 * Date  2023/3/6 下午 10:23
 */
@Mapper
public interface UserMapper {

    @Select("SELECT * FROM user WHERE user_id = #{userId}")
    User selectByUserId(@Param("userId") Integer userId);

    @Select("SELECT * FROM user WHERE login_name = #{loginName}")
    User selectByLoginName(@Param("loginName") String loginName);


    @Insert("INSERT INTO user(login_name, password) VALUES (#{loginName}, #{password})")
    int insertUser(@Param("loginName") String loginName, @Param("password") String password);

    @Update("UPDATE user SET login_name = #{loginName}, password = #{password}, introduce = #{introduce} WHERE user_id = #{userId}")
    int updateUserInfo(@Param("loginName") String loginName, @Param("password") String password, @Param("introduce") String introduce, @Param("userId") int userId);

}
