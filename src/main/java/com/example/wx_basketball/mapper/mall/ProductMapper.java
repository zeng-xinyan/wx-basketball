package com.example.wx_basketball.mapper.mall;

import com.example.wx_basketball.entity.mall.Product;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/9 上午 10:00
 */
@Mapper
public interface ProductMapper {

    List<Product> getProducts();

    Product getProductById(int productId);

}
