package com.example.wx_basketball.mapper.mall;

import com.example.wx_basketball.entity.mall.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ShoppingCartMapper {

    ShoppingCart getByProductId(@Param("productId") Integer productId, @Param("uid")  Integer uid);

    void add(@Param("productId") Integer productId, @Param("uid") Integer uid);

    void updateCount(@Param("productId") Integer productId, @Param("uid") Integer uid);

    void cutCount(@Param("productId") Integer productId, @Param("uid") Integer uid);

    void deleteByProductId(@Param("productId") Integer productId, @Param("uid") Integer uid);

    List getShoppingCartByUid(@Param("uid") Integer uid);
}
