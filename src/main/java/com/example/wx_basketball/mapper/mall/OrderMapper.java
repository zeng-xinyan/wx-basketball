package com.example.wx_basketball.mapper.mall;

import com.example.wx_basketball.entity.mall.Order;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface OrderMapper {

    @Insert("INSERT INTO mall_order(order_item, order_time, uid) VALUES (#{orderItem}, #{orderTime}, #{uid})")
    void add(@Param("orderItem") String orderItem, @Param("orderTime") String orderTime, @Param("uid") int uid);

    List<Order> list(@Param("uid") int uid);



}
