package com.example.wx_basketball.mapper;

import com.example.wx_basketball.entity.UserDynamic;
import org.apache.ibatis.annotations.*;

import java.util.Date;
import java.util.List;

/**
 * Author  zxy
 * Date  2023/3/7 下午 12:41
 */
@Mapper
public interface UserDynamicMapper {

    @Select("SELECT * FROM user_dynamic WHERE user_dynamic_id = #{userDynamicId}")
    UserDynamic getByUserDynamicId(@Param("userDynamicId") Integer userDynamicId);

    @Select("SELECT * FROM user_dynamic WHERE user_id = #{userId}")
    List<UserDynamic> getByUserId(@Param("userId") Integer userId);

    @Insert("INSERT INTO user_dynamic(user_id, dynamic, create_time) VALUES (#{userId}, #{dynamic}, #{date})")
    int insert(@Param("userId") Integer userId, @Param("dynamic") String dynamic, @Param("date") Date date);

    @Delete("DELETE FROM user_dynamic WHERE user_dynamic_id = #{userDynamicId}")
    int deleteByUserDynamicId(@Param("userDynamicId") Integer userDynamicId);

    @Update("UPDATE user_dynamic SET dynamic = #{dynamic} WHERE user_dynamic_id = #{userDynamicId}")
    int updateByUserDynamicId(@Param("userDynamicId") Integer userDynamicId, @Param("dynamic") String dynamic);
}
