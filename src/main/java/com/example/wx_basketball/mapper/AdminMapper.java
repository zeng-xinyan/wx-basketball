package com.example.wx_basketball.mapper;

import com.example.wx_basketball.entity.Admin;
import org.apache.ibatis.annotations.*;

/**
 * Author  zxy
 * Date  2023/3/6 下午 10:23
 */
@Mapper
public interface AdminMapper {

    @Select("SELECT * FROM admin_user WHERE login_name = #{loginName}")
    Admin selectByLoginName(@Param("loginName") String loginName);

}
