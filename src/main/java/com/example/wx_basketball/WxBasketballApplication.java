package com.example.wx_basketball;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WxBasketballApplication {

    public static void main(String[] args) {
        SpringApplication.run(WxBasketballApplication.class, args);
    }

}
