package com.example.wx_basketball.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * Author  zxy
 * Date  2023/3/7 下午 12:39
 */
public class UserDynamic {

    private int userDynamicId;

    private int userId;

    private String dynamic;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @Override
    public String toString() {
        return "UserDynamic{" +
                "userDynamicId=" + userDynamicId +
                ", userId=" + userId +
                ", dynamic='" + dynamic + '\'' +
                ", createTime=" + createTime +
                '}';
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getUserDynamicId() {
        return userDynamicId;
    }

    public void setUserDynamicId(int userDynamicId) {
        this.userDynamicId = userDynamicId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDynamic() {
        return dynamic;
    }

    public void setDynamic(String dynamic) {
        this.dynamic = dynamic;
    }
}
