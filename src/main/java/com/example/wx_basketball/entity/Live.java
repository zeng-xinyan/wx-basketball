package com.example.wx_basketball.entity;

/**
 * Author  zxy
 * Date  2023/3/8 下午 7:31
 */
public class Live {

    private int live_id;

    private String title;

    private String cover;

    private String url;

    @Override
    public String toString() {
        return "Live{" +
                "live_id=" + live_id +
                ", title='" + title + '\'' +
                ", cover='" + cover + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    public int getLive_id() {
        return live_id;
    }

    public void setLive_id(int live_id) {
        this.live_id = live_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
