package com.example.wx_basketball.entity.mall;

/**
 * Author  zxy
 * Date  2023/3/9 上午 9:20
 */
public class Order {

    private int orderId;

    private int uid;

    private String orderItem; // 订单对应的 JSON 数据。具体货物和数量

    private String orderTime;

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", uid=" + uid +
                ", orderItem='" + orderItem + '\'' +
                ", orderTime='" + orderTime + '\'' +
                '}';
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(String orderItem) {
        this.orderItem = orderItem;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }
}
