package com.example.wx_basketball.entity.mall;

/**
 * Author  zxy
 * Date  2023/3/9 上午 9:19
 */
public class Product {

    private int productId;

    private String title;

    private String subTitle;

    private String banner;

    private double price;

    private double oldPrice;

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", title='" + title + '\'' +
                ", subTitle='" + subTitle + '\'' +
                ", banner='" + banner + '\'' +
                ", price=" + price +
                ", oldPrice=" + oldPrice +
                '}';
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(double oldPrice) {
        this.oldPrice = oldPrice;
    }
}
