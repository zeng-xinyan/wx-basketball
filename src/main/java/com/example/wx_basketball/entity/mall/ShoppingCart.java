package com.example.wx_basketball.entity.mall;

/**
 * Author  zxy
 * Date  2023/3/9 上午 9:22
 */
public class ShoppingCart {

    private int cartId;

    private int uid;

    private int productId;

    private int count;

    private boolean hasBought;

    @Override
    public String toString() {
        return "ShoppingCart{" +
                "cartId=" + cartId +
                ", uid=" + uid +
                ", productId=" + productId +
                ", count=" + count +
                ", hasBought=" + hasBought +
                '}';
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isHasBought() {
        return hasBought;
    }

    public void setHasBought(boolean hasBought) {
        this.hasBought = hasBought;
    }
}
