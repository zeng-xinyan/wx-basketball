package com.example.wx_basketball.entity;

/**
 * Author  zxy
 * Date  2023/3/8 下午 4:17
 */
public class Video {

    private int videoId;

    private String url;

    private String intro;

    @Override
    public String toString() {
        return "Video{" +
                "videoId=" + videoId +
                ", url='" + url + '\'' +
                ", intro='" + intro + '\'' +
                '}';
    }

    public int getVideoId() {
        return videoId;
    }

    public void setVideoId(int videoId) {
        this.videoId = videoId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }
}
