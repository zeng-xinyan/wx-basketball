package com.example.wx_basketball.entity;

/**
 * Author  zxy
 * Date  2023/3/11 上午 9:10
 */
public class Admin {

    private int adminUserId;

    private String loginName;

    private String password;

    @Override
    public String toString() {
        return "Admin{" +
                "adminUserId=" + adminUserId +
                ", loginName='" + loginName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public int getAdminUserId() {
        return adminUserId;
    }

    public void setAdminUserId(int adminUserId) {
        this.adminUserId = adminUserId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
